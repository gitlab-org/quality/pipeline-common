## `gem-release` component

**!! DEPRECATED !! Please use the official component at https://gitlab.com/gitlab-org/components/gem-release !! DEPRECATED !!**

The component generates jobs that handle the release of a new gem version.

Upon a change to a specific pattern (defaults to `lib/**/version.rb`) in the default branch:

1. Release notes are computed from `Changelog:` Git trailers
1. Release (and tag) is created with these notes
1. Gem is built (native gems building is supported)
1. Gem is pushed to Rubygems

As long as the gem is located under the `gitlab-org/ruby/gems` group, there's no need to provide
a Rubygems.org API token (it's automatically set with the `GEM_HOST_API_KEY` CI/CD variable).

### Inputs

#### `gem_name`

The name of the gem to release, defaults to `$CI_PROJECT_NAME`.

#### `file_pattern_to_trigger_release`

The file pattern which triggers a new release, defaults to `"lib/**/version.rb"`.

#### `build_native_gems`

Whether to build native gems or not. Defaults to `false`.

#### `smoke_test_before_script`

Custom `before_script` (e.g. to install a native gem), defaults to `gem install ${GEM_FILE}`.

#### `smoke_test_script`

Custom `script` to run a smoke test on the built gem, defaults to `gem info ${GEM_FILE}`.

#### `dry_run`

Performs a dry-run (don't create a release nor push gems), defaults to `false`.
