# frozen_string_literal: true

require 'spec_helper'
require_relative '../script/push_mirror_check'

RSpec.describe MirrorCheck do
  let(:project_path) { 'project_path' }
  let(:gitlab_client) { instance_double('GitLab::Client') }
  let(:mirror_class) { Struct.new(:last_error, :update_status) }
  let(:last_error) { nil }
  let(:update_status) { 'finished' }
  let(:mirror) { [ mirror_class.new(last_error, update_status) ] }

  before do
    allow(Gitlab).to receive(:client).and_return(gitlab_client)
    allow(ENV).to receive(:[]).and_call_original
    allow(ENV).to receive(:[]).with('MIRROR_CHECK_GITLAB_API_TOKEN').and_return('token')
    allow(gitlab_client).to receive(:remote_mirrors)
      .with(project_path)
      .and_return(mirror)
  end

  subject { described_class.new(project_path) }

  shared_examples "status is not 'finished'" do
    it "fails with status message" do
      expect { MirrorCheck.run(project_path) }
        .to raise_error(RuntimeError, /Mirror status check failed with status of 'failed' and error: /)
    end
  end

  shared_examples "mirror errors" do
    it "fails with error message" do
      expect { MirrorCheck.run(project_path) }
        .to raise_error(RuntimeError, /Mirror status check failed with status of 'finished' and error: error/)
    end
  end

  shared_examples "mirror has status of 'finished' and no errors" do
    it "passes" do
      expect { MirrorCheck.run(project_path) }
        .to output(%r{Push mirror status check passed.}).to_stdout
    end
  end

  it_behaves_like "mirror has status of 'finished' and no errors"

  context "when no mirrors are found" do
    before do
      allow(gitlab_client).to receive(:remote_mirrors)
        .with(project_path)
        .and_return([])
    end

    it "aborts with message" do
      expect { MirrorCheck.run(project_path) }
        .to raise_error(RuntimeError, /No mirrors found!/)
    end
  end

  context "when mirror has an error" do
    let(:last_error) { 'error' }

    it_behaves_like "mirror errors"
  end

  context "when mirror has a status other than 'finished'" do
    let(:update_status) { 'failed' }

    it_behaves_like "status is not 'finished'"
  end

  context "with multiple mirrors where all have status of 'finished' and no errors" do
    let(:mirror) { [ mirror_class.new(last_error, update_status), mirror_class.new(last_error, update_status) ] }

    it_behaves_like "mirror has status of 'finished' and no errors"
  end

  context "with multiple mirrors where one has an error" do
    let(:mirror) { [ mirror_class.new(last_error, update_status), mirror_class.new(last_error, 'failed') ] }

    it_behaves_like "status is not 'finished'"
  end

  context "with multiple mirrors where one does not have a status of finished" do
    let(:mirror) { [ mirror_class.new('error', update_status), mirror_class.new(last_error, update_status) ] }

    it_behaves_like "mirror errors"
  end
end
