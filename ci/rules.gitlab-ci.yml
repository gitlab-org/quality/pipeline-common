.web: &web
  if: '$CI_PIPELINE_SOURCE == "web"'

.mr: &mr
  if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

.schedule: &schedule
  if: '$CI_PIPELINE_SOURCE == "schedule"'

.tag: &tag
  if: $CI_COMMIT_TAG

.trigger-smoke: &trigger-smoke
  if: '($CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline") && $SMOKE_ONLY == "true"'

.trigger-full: &trigger-full
  if: '($CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline") && $FULL_ONLY == "true"'

.schedule-smoke: &schedule-smoke
  if: '$CI_PIPELINE_SOURCE == "schedule" && $SMOKE_ONLY == "true"'

.schedule-full: &schedule-full
  if: '$CI_PIPELINE_SOURCE == "schedule" && $FULL_ONLY == "true"'

.trigger-schedule-smoke: &trigger-schedule-smoke
  if: '($CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "schedule") && $SMOKE_ONLY == "true"'

.trigger-schedule-full: &trigger-schedule-full
  if: '($CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "schedule") && $FULL_ONLY == "true"'

.schedule-geo: &schedule-geo
  if: '$CI_PIPELINE_SOURCE == "schedule" && $GEO_ONLY == "true"'

.schedule-epic-migration: &schedule-epic-migration
  if: '$CI_PIPELINE_SOURCE == "schedule" && $EPIC_SYNC_TEST == "true"'

.schedule-quarantine: &schedule-quarantine
  if: '$CI_PIPELINE_SOURCE == "schedule" && $QUARANTINE_ONLY == "true"'

.web-never: &web-never
  <<: *web
  when: never

# General resource cleanup scripts
.schedule-cleanup: &schedule-cleanup
  if: '$CI_PIPELINE_SOURCE == "schedule" && $RUN_CLEANUP == "true"'

.schedule-cleanup-single: &schedule-cleanup-single
  if: '$CI_PIPELINE_SOURCE == "schedule" && $RUN_CLEANUP == "true" && ($CLEANUP_ALL_QA_SANDBOX_GROUPS == null || $CLEANUP_ALL_QA_SANDBOX_GROUPS == "false")'

.schedule-cleanup-parallel: &schedule-cleanup-parallel
  if: '$CI_PIPELINE_SOURCE == "schedule" && $RUN_CLEANUP == "true" && $CLEANUP_ALL_QA_SANDBOX_GROUPS == "true"'

# Delayed saved test run resource cleanup
.scheduled-resources-cleanup: &scheduled-resources-cleanup
  if: '$CI_PIPELINE_SOURCE == "schedule" && $RESOURCES_CLEANUP == "true"'

.pipeline-common-ref-update-schedule: &pipeline-common-ref-update-schedule
  if: '$CI_PIPELINE_SOURCE == "schedule" && $UPDATE_INCLUDE_REF == "true"'

.trigger-canary-readiness: &trigger-canary-readiness
  if: '($CI_PIPELINE_SOURCE == "trigger" || $CI_PIPELINE_SOURCE == "pipeline") && ($CI_PROJECT_NAME == "canary" || $CI_PROJECT_NAME == "staging-canary")'

# ==========================================
# RULES
# ==========================================

# ------------------------------------------
# generic
# ------------------------------------------
.rules:mr:
  rules:
    - *mr

.rules:tag:
  rules:
    - *tag

# ------------------------------------------
# dot-com
# ------------------------------------------
# qa jobs
.com:qa:rules:trigger-schedule-smoke:
  rules:
    - <<: *trigger-smoke
      variables:
        QA_SAVE_TEST_METRICS: "true"
        KNAPSACK_GENERATE_REPORT: "true"
        CREATE_TEST_FAILURE_ISSUES: "true"
    - <<: *schedule-smoke
      variables:
        QA_SAVE_TEST_METRICS: "true"
        KNAPSACK_GENERATE_REPORT: "true"
        CREATE_TEST_FAILURE_ISSUES: "true"
    - <<: *mr
      when: manual
      allow_failure: true

.com:qa:rules:trigger-schedule-full:
  rules:
    - <<: *trigger-full
      variables:
        QA_SAVE_TEST_METRICS: "true"
        KNAPSACK_GENERATE_REPORT: "true"
        CREATE_TEST_FAILURE_ISSUES: "true"
    - <<: *schedule-full
      variables:
        QA_SAVE_TEST_METRICS: "true"
        KNAPSACK_GENERATE_REPORT: "true"
        CREATE_TEST_FAILURE_ISSUES: "true"
    - <<: *web
    - <<: *mr
      when: manual
      allow_failure: true

.com:qa:rules:schedule-geo:
  rules:
    - *schedule-geo
    - <<: *web
      when: manual
      allow_failure: true
    - <<: *mr
      when: manual
      allow_failure: true

.com:qa:rules:schedule-epic-migration:
  rules:
    - <<: *schedule-epic-migration

.com:qa:rules:quarantine-only:
  rules:
    - <<: *schedule-quarantine
      allow_failure: true
    - <<: *web
      when: manual
      allow_failure: true

.com:qa:rules:knapsack-report-upload:
  rules:
    - *web-never
    - *trigger-schedule-smoke
    - *trigger-schedule-full

# reports and notifications
.com:report:rules:trigger-schedule-test-runs:
  rules:
    - *web-never
    - *trigger-schedule-smoke
    - *trigger-schedule-full

# resource cleanup
.com:cleanup:rules:schedule-cleanup:
  rules:
    - *schedule-cleanup

.com:cleanup:rules:scheduled-resources-cleanup:
  rules:
    - *scheduled-resources-cleanup

.com:cleanup:rules:schedule-cleanup-single:
  rules:
    - *schedule-cleanup-single

.com:cleanup:rules:schedule-cleanup-parallel:
  rules:
    - *schedule-cleanup-parallel

# universal rules
.com:rules:trigger-schedule-test-runs:
  rules:
    - *trigger-schedule-smoke
    - *trigger-schedule-full
    - *schedule-quarantine
    - <<: *web
      variables:
        ALLURE_COPY_LATEST: "false"  # do not pollute history of master runs for manual one off triggers

# canary readiness check
.com:rules:canary-readiness-check:
  rules:
    - <<: *trigger-canary-readiness
      variables:
        CREATE_TEST_FAILURE_ISSUES: "false"
      when: always
      allow_failure: false
