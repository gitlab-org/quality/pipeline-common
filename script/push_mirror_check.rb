#!/usr/bin/env ruby

# frozen_string_literal: true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'gitlab', '~> 4.18'
end

# Helper class to check push mirror status of given project
# Example:
#   ruby script/push_mirror_check.rb gitlab-org/quality/staging
#
class MirrorCheck
  GITLAB_URL = 'https://gitlab.com'

  def initialize(project_path)
    @project_path = project_path || raise('Full path of project to check push mirrors for is required!')
    @gitlab = Gitlab.client(
      endpoint: "#{GITLAB_URL}/api/v4",
      private_token: ENV['MIRROR_CHECK_GITLAB_API_TOKEN'] || raise('MIRROR_CHECK_GITLAB_API_TOKEN must be set!')
    )
  end

  def self.run(project_path)
    new(project_path).check_push_mirrors
  end

  # @return [Gitlab::Client] gitlab client
  attr_reader :gitlab
  # @return [String] gitlab project to check push mirrors
  attr_reader :project_path

  def check_push_mirrors
    puts "Checking push mirror status for #{project_path}..."

    mirrors = gitlab.remote_mirrors(project_path)

    raise ("No mirrors found!") if mirrors.empty?

    mirrors.each do |mirror|
      puts 'Checking mirror...'

      next unless mirror.last_error || mirror.update_status != 'finished'

      raise("Mirror status check failed with status of '#{mirror.update_status}' and error: #{mirror.last_error}")
    end

    puts "Push mirror status check passed."
  end
end

if $PROGRAM_NAME == __FILE__
  MirrorCheck.run(*ARGV)
end
