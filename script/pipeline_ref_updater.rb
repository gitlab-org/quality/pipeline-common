#!/usr/bin/env ruby

# frozen_string_literal: true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'gitlab', '~> 4.18'
end

# Helper class to update pipeline-common ref to latest available tag
# Example:
#   ruby script/pipeline_ref_updater.rb gitlab-org/quality/staging .gitlab-ci.yml
#
class Updater
  GITLAB_URL = 'https://gitlab.com'
  PIPELINE_PROJECT_NAME = 'gitlab-org/quality/pipeline-common'

  def initialize(project_path, ci_yml_path, ref)
    @project_path = project_path || raise('Full path of project to update is required!')
    @ci_yml_path = ci_yml_path
    @gitlab = Gitlab.client(
      endpoint: "#{GITLAB_URL}/api/v4",
      private_token: ENV['REF_UPDATE_GITLAB_API_TOKEN'] || raise('REF_UPDATE_GITLAB_API_TOKEN must be set!')
    )
    @ref = ref
  end

  def self.run(project_path, ci_yml_path = '.gitlab-ci.yml', ref = 'master')
    new(project_path, ci_yml_path, ref).update
  end

  def update
    raise('Unable to detect currently used version!') unless current_version
    return puts('Version is already up to date!') if latest_version == current_version

    puts "Updating pipeline-common: #{current_version} => #{latest_version}"
    return puts(' merge request already exists, quitting...') if merge_request_exists?

    create_branch unless branch_exists?
    create_commit unless commit_exists?
    create_merge_request
  end

  private

  # @return [Gitlab::Client] gitlab client
  attr_reader :gitlab
  # @return [String] gitlab project to update
  attr_reader :project_path
  # @return [String] path to ci yml file
  attr_reader :ci_yml_path
  # @return [String] ref
  attr_reader :ref

  # Create update merge request
  #
  # @return [void]
  def create_merge_request
    puts "  creating merge request: '#{commit_message}'"
    mr = gitlab.create_merge_request(
      project_path,
      commit_message,
      description: mr_description,
      source_branch: branch_name,
      target_branch: project.default_branch,
      remove_source_branch: true,
      labels: 'Quality,type::maintenance,maintenance::pipelines'
    )

    puts "  successfully created mr: #{mr.web_url}"
  end

  # Check if merge request exists
  #
  # @return [Boolean]
  def merge_request_exists?
    gitlab.merge_requests(
      project_path,
      source_branch: branch_name,
      target_branch: project.default_branch,
      state: 'all'
    ).any?
  end

  # Latest release version
  #
  # @return [String]
  def latest_version
    @latest_version ||= gitlab.tags(PIPELINE_PROJECT_NAME, order_by: 'updated').map(&:name).first
  end

  # Current pipeline-common ref
  #
  # @return [String]
  def current_version
    @current_version ||= YAML.safe_load(ci_yml, symbolize_names: true, aliases: true)
                             .fetch(:include, [])
                             .find { |includes| includes[:project] == PIPELINE_PROJECT_NAME }
                             .fetch(:ref, nil)
  end

  # Create branch
  #
  # @return [void]
  def create_branch
    puts "  creating branch: '#{branch_name}'"
    gitlab.create_branch(project_path, branch_name, ref)
  end

  # Check if branch already exists
  #
  # @return [Boolean]
  def branch_exists?
    gitlab.branch(project_path, branch_name)
  rescue Gitlab::Error::NotFound
    false
  end

  # Branch name
  #
  # @return [String]
  def branch_name
    @branch_name ||= "update-ref-to-#{latest_version}"
  end

  # Create commit with updated ref
  #
  # @return [void]
  def create_commit
    actions = [{
      action: 'update',
      file_path: ci_yml_path,
      content: ci_yml.gsub(current_version, latest_version)
    }]

    puts "  creating update commit: '#{commit_message}'"
    gitlab.create_commit(project_path, branch_name, commit_message, actions)
  end

  # Check if commit exists
  #
  # @return [Boolean]
  def commit_exists?
    gitlab.commits(project_path, ref_name: branch_name)&.first&.message == commit_message
  end

  # Commit/MR description
  #
  # @return [String]
  def commit_message
    @commit_message ||= "Update pipeline-common ref to #{latest_version}"
  end

  # Update merge request description
  #
  # @return [String]
  def mr_description
    @mr_description ||= <<~DSC
      Updates [pipeline-common](#{GITLAB_URL}/#{PIPELINE_PROJECT_NAME}) ref from `#{current_version}` to `#{latest_version}`

      <details>
      <summary>Changelog</summary>

      #{changelog}
      </details>

      <details>
      <summary>Commits</summary>
      #{GITLAB_URL}/#{PIPELINE_PROJECT_NAME}/-/compare/#{current_version}...#{latest_version}
      </details>

      Merge request should be good to merge as long as pipeline for this update passes.

      cc: @gl-dx/maintainers
    DSC
  end

  # Changelog for the release
  #
  # @return [String]
  def changelog
    releases = gitlab.project_releases(PIPELINE_PROJECT_NAME)
    current_index = releases.index { |release| release.tag_name == current_version }

    releases[0..current_index - 1]
      .map { |release| "<blockquote>\n\n#{release.description.strip}\n</blockquote>" }
      .join("\n\n")
  end

  # Gitlab project
  #
  # @return [Gitlab::ObjectifiedHash]
  def project
    @project ||= gitlab.project(project_path)
  end

  # Contents of .gitlab-ci.yml
  #
  # @return [String]
  def ci_yml
    @ci_yml ||= gitlab.file_contents(project_path, ci_yml_path, ref)
  end
end

Updater.run(*ARGV)
