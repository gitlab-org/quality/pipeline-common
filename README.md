# Common pipeline configuration files

[[_TOC_]]

This project has common pipeline config files that can be reused in multiple projects:

- E2E pipeline configurations that can be used in live environment projects
- Allure report template (deprecated in favor of [the `allure-report` component](templates/allure-report))
- Danger template
- Gem release template (deprecated in favor of [the `gem-release` component](templates/gem-release))

## Live E2E environment pipeline configurations

[README.md](ci/dot-com/README.md)

## CI/CD Components

Some pipeline configurations have been transitioned to [CI/CD components](https://docs.gitlab.com/ee/ci/components/):

- [`allure-report` component](templates/allure-report)
- [`gem-release` component](templates/gem-release)

## Testing the changes in this project

[Testing the changes in pipeline-common](docs/testing_changes.md)

## Release process

Changes to templates should be released in same fashion as any other dependency.

### Versioning

We follow [Semantic Versioning](https://semver.org) (SemVer).
In short, this means that the new version should reflect the types of changes that are about to be released.

*summary from semver.org*

MAJOR.MINOR.PATCH

- MAJOR version when you make backwards incompatible changes
- MINOR version when you add functionality in a backwards compatible manner
- PATCH version when you make backwards compatible bug fixes.

### When we release

We release `pipeline-common` on an ad-hoc basis. There is no regularity to when we release, we just release
when we make a change - no matter the size of the change.

### How-to

Create a SemVer compatible tag from [the tags page](https://gitlab.com/gitlab-org/quality/pipeline-common/-/tags)
with a "Release x.y.z" message.

### Changelog

We use the [Changelog](https://docs.gitlab.com/ee/development/changelog.html#overview) Git trailers
for marking commits for automated changelog entry generation.

### Flowchart

The flowchart below outlines a high-level overview of the `pipeline-common` release process.

The **QA environment projects** subgraph represents each of the different projects that reference `pipeline-common`, as listed above in the [QA pipeline types section](#qa-pipeline-types).

#### Key:
- Rectangles represent manual actions
- Hexagons represent automated actions

```mermaid
flowchart LR
    subgraph pipeline-common
      A[Create new tag] -- Release pipeline triggered --> B{{Release created}}
    end
    subgraph qa-projects[QA environment projects]
      C{{Daily pipeline-common version update job runs}} --> D{{MR opened}}
      D --> E[Review and merge] --> F{{Ref updated}}
    end
    pipeline-common --> qa-projects
```

## Automated updates

[`ref-update.gitlab-ci.yml`](ci/ref-update.gitlab-ci.yml) defines a job which will automatically check if a newer version of `pipeline-common`
is released and create merge request updating `ref` value in `.gitlab-ci.yml`.

Job requires following setup:

- `REF_UPDATE_GITLAB_API_TOKEN` environment variable with gitlab api token if outside `Quality Department` group
- [Schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) job with variable `UPDATE_INCLUDE_REF` set to `true`
- [Include](https://docs.gitlab.com/ee/ci/yaml/#includefile) definition with `ci/ref-update.gitlab-ci.yml`
- `update` [stage](https://docs.gitlab.com/ee/ci/yaml/#stages) present in ci configuration of the project receiving updates
- `CI_YML_PATH` set to the path of the file that includes files from this project (default to `.gitlab-ci.yml`)

## Lefthook

[Lefthook](https://github.com/evilmartians/lefthook) is a Git hooks manager that allows
custom logic to be executed prior to Git committing or pushing. This project comes with
Lefthook configuration checked in (`lefthook.yml`), but it must be installed.

### Install Lefthook

   ```shell
   # Install the `lefthook` Ruby gem:
   bundle install
   # Initialize the lefthook config and adds to .git/hooks dir
   bundle exec lefthook install
   # Verify hook execution works as expected
   bundle exec lefthook run pre-push
   ```

For a detailed guide on left hook configuration see https://github.com/evilmartians/lefthook/blob/master/docs/configuration.md
