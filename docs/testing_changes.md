# Testing changes in `pipeline-common`

Changes pushed to the pipeline-common project can be tested in the relevant projects that references the code from the
pipeline-common project. Testing these changes involve creating a new branch in the relevant project that use the
pipeline-common code and starting a new pipeline in that project using that branch.

For example, assuming that the name of the branch that has the changes in pipeline-common project is
"my-branch-in-pipeline-common", create a new branch in the relevant project (say "my-branch-in-relevant-project") that includes the `gitlab-org/quality/pipeline-common`
project and update the `ref` for the pipeline-common project to the branch "my-branch-in-pipeline-common".

For example:
```yaml
include:
  - project: gitlab-org/quality/pipeline-common
    ref: "my-branch-in-pipeline-common"
    file:
      - /ci/dot-com/qa.gitlab-ci.yml
      - /ci/dot-com/reports.gitlab-ci.yml
      - /ci/dot-com/cleanup.gitlab-ci.yml
      - /ci/ref-update.gitlab-ci.yml
```

Next, start a new pipeline that will have the jobs that need to be tested. The manner in which the new
pipeline is started depends what jobs need to be tested.

There are multiple ways a pipeline can be started depending on the jobs to be tested:

1. If the jobs to be tested are scheduled jobs, create and run a new temporary inactive pipeline schedule in the relevant project using the "my-branch-in-relevant-project" branch
and setting the required environment variables. Example scheduled pipelines and variables can be seen [here](https://ops.gitlab.net/gitlab-org/quality/staging/-/pipeline_schedules).
1. If the jobs to be tested are triggered jobs, use [the pipeline trigger API](https://docs.gitlab.com/ee/ci/triggers) to start a new pipeline. You will need to set the `ref` to "my-branch-in-relevant-project".
1. If the jobs to be tested are MR jobs, just create a new MR from "my-branch-in-relevant-project" branch. This branch might need additional code changes to trigger the jobs that needs to be tested.
